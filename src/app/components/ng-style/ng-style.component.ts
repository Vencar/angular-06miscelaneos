import { Component, OnInit } from '@angular/core';

@Component({
  selector: "app-ng-style",
  template: `
    <div class="container">
      <div class="row">
        <p [style.fontSize.px]="tamano">
          Hola mundo... esta es una etiqueta
        </p>
      </div>
      <div class="row">
          <button class="btn btn-primary" (click)="tamano = tamano + 5">
            <i class="fa fa-plus"></i>
          </button>
          <button class="btn btn-danger" (click)="tamano = tamano - 5">
            <i class="fa fa-minus"></i>
          </button>
      </div>
    </div>
  `,
  styles: []
})
export class NgStyleComponent implements OnInit {
  tamano: number = 10;

  constructor() {}

  ngOnInit() {}
}
